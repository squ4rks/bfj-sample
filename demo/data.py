# Copyright (c) 2022 Cisco and/or its affiliates.
#
# This software is licensed to you under the terms of the Cisco Sample
# Code License, Version 1.1 (the "License"). You may obtain a copy of the
# License at
#
#               https://developer.cisco.com/docs/licenses
#
# All use of the material herein must be in accordance with the terms of
# the License. All rights not expressly granted by the License are
# reserved. Unless required by applicable law or agreed to separately in
# writing, software distributed under the License is distributed on an "AS
# IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
# or implied.

import json
from genie.testbed import load

testbed = load('testbed.yaml')

# Read mapping of tags to commands
mapping = json.load(open("mapping.json"))
for dev_name, dev in testbed.devices.items():
    # Skip non IOS XE devices in testbed
    if dev.os != "iosxe":
        continue
    
    # Connect to device and parse command
    dev.connect(log_stdout=False)
    out = dev.parse('show interfaces description')

    # Loop over all interfaces and extract their description
    for intf_name in out['interfaces']:
        intf = out['interfaces'][intf_name]
        descr = intf['description']

        # Check if any of the tags are in description
        for tag in mapping.keys():
            if tag in descr:
                print(f"{tag} found in {intf_name}")

                # Assemble config commands to be send
                config_commands = [f"interface {intf_name}"]
                config_commands.extend(mapping[tag])

                # Apply commands to interface
                dev.config(config_commands)

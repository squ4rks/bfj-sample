# Bfj Sample

## Demos

* Link zu den Demos aus der Präsentation inklusive Quellcode und Erklärungen: https://github.com/sQu4rks/DEVNET-1970/ und https://github.com/sQu4rks/DEVNET-1439.
* Der Quellcode für die Interface Demo ist im Ordner [demo](demo/) zu finden.

## Resourcen

* pyATS Einführung: https://developer.cisco.com/docs/pyats-getting-started/
* NSO Evaluationslizenz: https://developer.cisco.com/docs/nso/#!getting-and-installing-nso/getting-nso

## Sandboxes

* DevNet Sandboxes: https://devnetsandbox.cisco.com/

